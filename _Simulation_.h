#ifndef _Simulation__h
#define _Simulation__h

#include<iostream>
#include<random>
#include<unistd.h>
#include<time.h>

#include"_SQLite_Database_.h"
#include"_MPI_vector_.h"

class _Simulation_{
	private:
 std::mt19937 rand32;
 std::mt19937_64 rand64;
 std::uniform_real_distribution<double> dist;
 unsigned int L;
 static const unsigned int NParam = 2;
 static const unsigned int NResults = 2;
	public:
 void *Lattice;
 void Observables;
 _MPI_vector_<double> Results, Parameters;
 
 _Simulation_(void);
 _Simulation_(unsigned int, unsigned long long);
 _Simulation_& Seed(unsigned long long);
 _Simulation_& Set_Parameters(double*);
 _Simulation_& Set_Parameters(_MPI_vector_<double>);
 _Simulation_& Set_InitialConditions(void);
 _Simulation_& Simulate(void);
 void AllocateLattice(unsigned int);
 

};

/*************************************

	CONSTRUCTORS

*************************************/

_Simulation_::_Simulation_(void):
	dist(0.0, 1.0)
{}

_Simulation_::_Simulation_(unsigned int L, unsigned long long seed):
	dist(0.0, 1.0)
{
 this->AllocateLattice(L);
 this->Seed(seed);
}

_Simulation_& _Simulation_::Seed(unsigned long long seed){
 rand32.seed(seed);
 rand64.seed(seed);
 return *this;
}

void _Simulation_::AllocateLattice(unsigned int L){
 this->L = L;
}


/*************************************

	GET PRIVATE DATA

*************************************/

_Simulation_& _Simulation_::Set_Parameters(double *Param){
 for(int index = 0; index < Parameters.Get_size(); index++){
	Parameters[index] = Param[index];
 }
 return *this;
}

_Simulation_& _Simulation_::Set_Parameters(_MPI_vector_<double> Param){
 Parameters = Param;
 return *this;
}


/*************************************

	Simulate

*************************************/

_Simulation_& _Simulation_::Set_InitialConditions(void){

 return *this;
}

_Simulation_& _Simulation_::Simulate(void){

 return *this;
}

#endif
