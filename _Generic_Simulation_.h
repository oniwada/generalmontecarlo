#ifndef _GENERIC_SIMULATION__H
#define _GENERIC_SIMULATION__H
#include<iostream>
#include"_MPI_vector_.h"
#include"_SQLite_Database_.h"

class _Generic_Simulation_{
	public:
 _MPI_vector_<double> *P2_Param, *P2_Res;
 _SQLite_Database_ *P2_Save;
 virtual _Generic_Simulation_& Set_Parameters(){return *this;}
 virtual bool Set_InitialConditions() {return false;}
 virtual bool Set_InitialConditions(double) {return false;}
 virtual _Generic_Simulation_& Simulate() {return *this;}
 virtual _Generic_Simulation_& Save_Simulation(long long) {return *this;}
 virtual long long Init_Database() {return -1L;}
 virtual int Get_SimulationState() {return -1;}
 virtual void BeginTransaction(void) {}
 virtual void EndTransaction( void) {}

 _Generic_Simulation_* operator+();
};

_Generic_Simulation_* _Generic_Simulation_::operator+(){
 return this;
}

#endif
