#ifndef _Parallelize_TimeSeries__H
#define _Parallelize_TimeSeries__H

#include<iostream>
#include<mpi.h>
#include<assert.h>
#include<math.h>
#include"_Generic_Simulation_.h"
#include"_Time_Series_.h"

class _Parallelize_TimeSeries_{
	private:
 _Generic_Simulation_ *Par_Simul;
 _Time_Series_ TMP_Serie;
 int rank = -1;
 int size = -1;
 unsigned long long NTS_perSlave = 4096;
 FILE *GetParam = NULL;

 void master(void);
 void slave(void);
 void Save_TimeSeries(void);
	public:
 inline unsigned long long Get_NTS_perSlave(void);
 inline void Set_NTS_perSlave(unsigned long long);
 _Parallelize_TimeSeries_(_Generic_Simulation_*, const char*, int, int);
 _Parallelize_TimeSeries_(_Generic_Simulation_*, int, int);
 void run(void);
};

_Parallelize_TimeSeries_::_Parallelize_TimeSeries_(_Generic_Simulation_ *ToParal, const char *filename,int rank, int size):
	TMP_Serie(*ToParal, 32)
{
 Par_Simul = ToParal;
 assert(rank >= 0 && size > 0);
 this->rank = rank;
 this->size = size;
 if( rank == 0 ){
	GetParam = fopen(filename, "r");
	assert(GetParam != NULL && "Error opening file");
 }
}

/******************************************************
	CONSTRUCTORS DESTRUCTORS
******************************************************/

_Parallelize_TimeSeries_::_Parallelize_TimeSeries_(_Generic_Simulation_ *ToParal, int rank, int size):
	TMP_Serie(*ToParal, 32)
{
 Par_Simul = ToParal;
 assert(rank >= 0 && size > 0);
 this->rank = rank;
 this->size = size;
}


/******************************************************
	GET AND SET
******************************************************/

inline unsigned long long _Parallelize_TimeSeries_::Get_NTS_perSlave(void){
 return NTS_perSlave;
}

inline void _Parallelize_TimeSeries_::Set_NTS_perSlave(unsigned long long newNTS_perSlave){
 NTS_perSlave = newNTS_perSlave;
}
/******************************************************
	PARALLELIZE THE SIMULATION
******************************************************/

void _Parallelize_TimeSeries_::run(void){
 if( rank == 0){
	master();
 }
 else{
	slave();
 }
}

void _Parallelize_TimeSeries_::master(void){
 double NSimul;
 _MPI_vector_<double> Param_Send(Par_Simul->P2_Param->Get_size());
 
 if(Param_Send.Scan(GetParam, "%lf", "%*c") == EOF){
	fprintf(stderr, "nothing to read\n");
	exit(1);
 }
 if(fscanf(GetParam, "%*c%lf", &NSimul) == EOF){
	fprintf(stderr, "nothing to read\n");
	exit(1);
 }
 NSimul = (NSimul - fmod(NSimul, NTS_perSlave))/double(NTS_perSlave);
 for(int Process = 1; Process < size; Process++){
	if(NSimul - Process + 1.0 > 0.0){
		Param_Send.Send(Process, 0);
	}
	else{
		double MoreSimul;
		if(Param_Send.Scan(GetParam, "%lf", "%*c") == EOF){
			Param_Send[0] = -1.0;
		}
		if(fscanf(GetParam, "%*c%lf", &MoreSimul) == EOF){
			Param_Send[0] = -1.0;
			MoreSimul = 0.0;
		}		
		MoreSimul = (MoreSimul - fmod(MoreSimul, NTS_perSlave))/double(NTS_perSlave);
		NSimul += MoreSimul;
		Param_Send.Send(Process, 0);
	}
 }
 NSimul += 1.0 - double(size);
 while(NSimul > 0.0){
	NSimul -= 1.0;
	Par_Simul->P2_Param->SleepRecvAny();
	TMP_Serie.Recv_TimeSeries(Par_Simul->P2_Param->Status.MPI_SOURCE, Par_Simul->P2_Param->Status.MPI_TAG);
	Param_Send.Send(Par_Simul->P2_Param->Status.MPI_SOURCE, Par_Simul->P2_Param->Status.MPI_TAG);
	Save_TimeSeries();
 }
 while(Param_Send.Scan(GetParam, "%lf", "%*c") != EOF){
	if(fscanf(GetParam, "%*c%lf", &NSimul) == EOF) break;
	NSimul = (NSimul - fmod(NSimul, NTS_perSlave))/double(NTS_perSlave);
	while(NSimul > 0.0){
		NSimul -= 1.0;
		Par_Simul->P2_Param->SleepRecvAny();
		TMP_Serie.Recv_TimeSeries(Par_Simul->P2_Param->Status.MPI_SOURCE, Par_Simul->P2_Param->Status.MPI_TAG);
		Param_Send.Send(Par_Simul->P2_Param->Status.MPI_SOURCE, Par_Simul->P2_Param->Status.MPI_TAG);
		Save_TimeSeries();
	}	
 }
 Param_Send[0] = -1.0;
 for(int Process = 1; Process < size; Process++){
	Par_Simul->P2_Param->SleepRecvAny();
	TMP_Serie.Recv_TimeSeries(Par_Simul->P2_Param->Status.MPI_SOURCE, Par_Simul->P2_Param->Status.MPI_TAG);
	Param_Send.Send(Par_Simul->P2_Param->Status.MPI_SOURCE, Par_Simul->P2_Param->Status.MPI_TAG);
	Save_TimeSeries();
 }
 printf("I'm the master process!!\n");
}

/*******************************
0 - L
1 - lambda
2 - p
3 - t / t_init
4 - t_max
5 - tau
*******************************/

void _Parallelize_TimeSeries_::slave(void){
 Par_Simul->P2_Param->Recv(0, 0);
 while((*Par_Simul->P2_Param)[0] >= 0.0){
	unsigned long long Niter = (*Par_Simul->P2_Param)[4]/((*Par_Simul->P2_Param)[5]);
	(*Par_Simul->P2_Param)[4] = (*Par_Simul->P2_Param)[3];
	while(Niter > (unsigned long long) TMP_Serie.Get_Res_ArraySize()){
		TMP_Serie.Realloc_TimeSeries(2*TMP_Serie.Get_Res_ArraySize());
	}
	TMP_Serie.Res_Array.Set_to_0();
	for(unsigned long long Realization = 0; Realization < NTS_perSlave; Realization++){
		Par_Simul->Set_Parameters();
		Par_Simul->Set_InitialConditions();
		Par_Simul->Simulate();
		for(unsigned long long iter = 0; iter < Niter; iter++){
			Par_Simul->Simulate();
			(*Par_Simul->P2_Param)[3] += (*Par_Simul->P2_Param)[5];
			TMP_Serie.Add_line(*Par_Simul->P2_Res, iter);
		}
		(*Par_Simul->P2_Param)[3] = (*Par_Simul->P2_Param)[4];
	}
	(*Par_Simul->P2_Param)[3] = (*Par_Simul->P2_Param)[4];
	(*Par_Simul->P2_Param)[4] = 0.0;
	Par_Simul->P2_Param->Send(0, 0);
	TMP_Serie.Send_Partial_TimeSeries(0, 0, Niter);
	Par_Simul->P2_Param->Recv(0, 0);
 }
 printf("just the %dth slave ..\n", rank);
}

void _Parallelize_TimeSeries_::Save_TimeSeries(void){
 double tau = (*Par_Simul->P2_Param)[5];
 (*Par_Simul->P2_Param)[5] = 0.0;
 for(int time = 0; time < TMP_Serie.Get_Received_size(); time++, (*Par_Simul->P2_Param)[3] += tau){
	TMP_Serie.Get_line(*Par_Simul->P2_Res, time);
	Par_Simul->Save_Simulation();
 }
}

#endif
