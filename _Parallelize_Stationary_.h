#ifndef _PARALLELIZE_STATIONARY__H
#define _PARALLELIZE_STATIONARY__H

#include<iostream>
#include<mpi.h>
#include<assert.h>
#include"_Generic_Simulation_.h"

class _Parallelize_Stationary_{
	private:
 _Generic_Simulation_ *Par_Simul;
 int rank = -1;
 int size = -1;
 FILE *GetParam = NULL;

 void master(void);
 void slave(void);
	public:
 _Parallelize_Stationary_(_Generic_Simulation_*, const char*, int, int);
 _Parallelize_Stationary_(_Generic_Simulation_*, int, int);
 void run(void);
};

_Parallelize_Stationary_::_Parallelize_Stationary_(_Generic_Simulation_ *ToParal, const char *filename,int rank, int size){
 Par_Simul = ToParal;
 assert(rank >= 0 && size > 0);
 this->rank = rank;
 this->size = size;
 if( rank == 0 ){
	GetParam = fopen(filename, "r");
	assert(GetParam != NULL && "Error opening file");
 }
}

_Parallelize_Stationary_::_Parallelize_Stationary_(_Generic_Simulation_ *ToParal, int rank, int size){
 Par_Simul = ToParal;
 assert(rank >= 0 && size > 0);
 this->rank = rank;
 this->size = size;
}

void _Parallelize_Stationary_::run(void){
 if( rank == 0){
	master();
 }
 else{
	slave();
 }
}

void _Parallelize_Stationary_::master(void){
 double NSimul;
 _MPI_vector_<double> Param_Send(Par_Simul->P2_Param->Get_size());
 
 if(Param_Send.Scan(GetParam, "%lf", "%*c") == EOF){
	fprintf(stderr, "nothing to read\n");
	exit(1);
 }
 if(fscanf(GetParam, "%*c%lf", &NSimul) == EOF){
	fprintf(stderr, "nothing to read\n");
	exit(1);
 }
 for(int Process = 1; Process < size; Process++){
	if(NSimul - Process + 1.0 > 0.0){
		Param_Send.Send(Process, 0);
	}
	else{
		double MoreSimul;
		if(Param_Send.Scan(GetParam, "%lf", "%*c") == EOF){
			Param_Send[0] = -1.0;
		}
		if(fscanf(GetParam, "%*c%lf", &MoreSimul) == EOF){
			Param_Send[0] = -1.0;
			MoreSimul = 0.0;
		}
		NSimul += MoreSimul;
		Param_Send.Send(Process, 0);
	}
 }
 NSimul += 1.0 - double(size);
 while(NSimul > 0.0){
	NSimul -= 1.0;
	Par_Simul->P2_Res->SleepRecvAny();
	Par_Simul->P2_Param->Recv(Par_Simul->P2_Res->Status.MPI_SOURCE, Par_Simul->P2_Res->Status.MPI_TAG);
	Par_Simul->Save_Simulation();
	Param_Send.Send(Par_Simul->P2_Res->Status.MPI_SOURCE, Par_Simul->P2_Res->Status.MPI_TAG);
 }
 while(Param_Send.Scan(GetParam, "%lf", "%*c") != EOF){
	if(fscanf(GetParam, "%*c%lf", &NSimul) == EOF) break;
	while(NSimul > 0.0){
		NSimul -= 1.0;
		Par_Simul->P2_Res->SleepRecvAny();
		Par_Simul->P2_Param->Recv(Par_Simul->P2_Res->Status.MPI_SOURCE, Par_Simul->P2_Res->Status.MPI_TAG);
		Par_Simul->Save_Simulation();
		Param_Send.Send(Par_Simul->P2_Res->Status.MPI_SOURCE, Par_Simul->P2_Res->Status.MPI_TAG);
	}	
 }
 Param_Send[0] = -1.0;
 for(int Process = 1; Process < size; Process++){
	Par_Simul->P2_Res->SleepRecvAny();
	Par_Simul->P2_Param->Recv(Par_Simul->P2_Res->Status.MPI_SOURCE, Par_Simul->P2_Res->Status.MPI_TAG);
	Par_Simul->Save_Simulation();
	Param_Send.Send(Par_Simul->P2_Res->Status.MPI_SOURCE, Par_Simul->P2_Res->Status.MPI_TAG);
 }
 printf("I'm the master process!!\n");
}

void _Parallelize_Stationary_::slave(void){
 Par_Simul->P2_Param->Recv(0, 0);
 _MPI_vector_<double> Means(Par_Simul->P2_Res->Get_size());
 while((*Par_Simul->P2_Param)[0] >= 0.0){
	Means = 0.0;
	Par_Simul->Set_Parameters();
	Par_Simul->Set_InitialConditions();
	Par_Simul->Simulate();
	for(int iter = 0; iter < (int) (*Par_Simul->P2_Param)[4]; iter++){
		(*Par_Simul->P2_Param)[3] += (*Par_Simul->P2_Param)[5];
		Par_Simul->Simulate();
		Means += *Par_Simul->P2_Res;
	}
	Means /= (*Par_Simul->P2_Param)[4];
	(*Par_Simul->P2_Param)[3] -= (*Par_Simul->P2_Param)[5]*(*Par_Simul->P2_Param)[4];
	Means.Send(0, 0);
	Par_Simul->P2_Param->Send(0, 0);
	Par_Simul->P2_Param->Recv(0, 0);
 }
 printf("just the %dth slave ..\n", rank);
}

#endif
