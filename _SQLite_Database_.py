# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 17:00:24 2015

@author: chuchu-beleza
"""

import sqlite3

class _SQLite_Database_:
    def __init__(self, Database, TableName):
        self.Connection = sqlite3.connect(Database)
        self.Data = self.Connection.cursor()
        self.TableName = TableName
        self.PS = "{:.14f}"
        self.ColumnsPrimaryKey = self.Get_PrimaryKey()
        self.Values = self.Get_Values()
        
        
    def Exec(self, Command):
        temp = []
        for tup in self.Data.execute(Command):
            temp.append(list(tup))
        return temp
        
    def GenerateWhereEqual(self, PrimaryKey, Delta):
	Command = "WHERE "
        sizeCPK = len(self.ColumnsPrimaryKey)-1
        for i in range(sizeCPK):
            Command = Command + self.ColumnsPrimaryKey[i] + ">" + self.PS.format(PrimaryKey[i]-Delta[i]) + " AND "
            Command = Command + self.ColumnsPrimaryKey[i] + "<" + self.PS.format(PrimaryKey[i]+Delta[i]) + " AND "
        Command = Command + self.ColumnsPrimaryKey[sizeCPK] + ">" + self.PS.format(PrimaryKey[sizeCPK]-Delta[sizeCPK]) + " AND "
        Command = Command + self.ColumnsPrimaryKey[sizeCPK] + "<" + self.PS.format(PrimaryKey[sizeCPK]+Delta[sizeCPK])
	return Command

    def Searchfor(self, PrimaryKey, Delta):
        Command = "SELECT "
        sizeCPK = len(self.ColumnsPrimaryKey)-1
        for i in range(len(self.Values) -1):
            Command = Command + self.Values[i] + ","
        Command = Command + self.Values[len(self.Values)-1] + " FROM " + self.TableName + " "
	Command = Command + self.GenerateWhereEqual(PrimaryKey, Delta) + ";"
	return self.Exec(Command)
        
    def SearchforAll(self, PrimaryKey, Delta):
        Command = "SELECT * "
        Command = Command + "FROM " + self.TableName + " "
	Command = Command + self.GenerateWhereEqual(PrimaryKey, Delta) + ";"
	return self.Exec(Command)

    def Get_PrimaryKey(self):
        temp = []
        for Information in self.Exec("PRAGMA TABLE_INFO(" + self.TableName + ")"):
            if Information[5] != 0:
                temp.append(Information[1])
        return temp;
        
    def Get_Values(self):
        temp = []
        for Information in self.Exec("PRAGMA TABLE_INFO(" + self.TableName + ")"):
            if Information[5] == 0:
                temp.append(Information[1])
        return temp;

    def Commit(self):
	self.Connection.commit()
