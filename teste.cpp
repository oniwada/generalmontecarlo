#include<iostream>
#include<random>
#include<unistd.h>
#include<time.h>
#include<math.h>
#include"_MPI_vector_.h"
#include"_SQLite_Database_.h"
#include"_Pi_.h"

void master(char *StringInput[], int size, int rank){

 unsigned long long NSimul;
 sscanf(StringInput[2], "%llu", &NSimul);

 _Pi_ JustParam(StringInput[0], StringInput[1]);
 JustParam.Parameters[0] = 0.19;
 
 if(NSimul < JustParam.NSimul){
	fprintf(stderr, "low number of simulations\n");
	exit(1);
 }

 std::mt19937_64 SeedGen(time(NULL));
 for(int Process = 1; Process < size; Process++){
	_MPI_vector_<unsigned long long> temp(2);
	temp[0] = SeedGen();
	temp[1] = SeedGen();
	temp.Send(Process, 0);
 }

 unsigned long long NSend, NRecv;
 NSend = NRecv = 0;
 int Process = 1;
 for(; Process < size; Process++){
	if(NSend >= NSimul) break;
	NSend += JustParam.NSimul;
	JustParam.Parameters.Send(Process, 0);
 }
 _MPI_vector_<double> Finish;
 Finish = JustParam.Parameters;
 double chuchu =-0.1;
 Finish = chuchu;
 for(; Process < size; Process++){
	Finish.Send(Process, 0);
 }
 
 while(NRecv < NSimul){
	NRecv += JustParam.NSimul;
	JustParam.Results.SleepRecvAny();
	JustParam.SaveSimulation();
//	JustParam.Results.Print(stdout, "%le", " ");
//	printf("\n");
	if(NSend < NSimul){
		NSend += JustParam.Results[0];
		JustParam.Parameters.Send(JustParam.Results.Status.MPI_SOURCE, JustParam.Results.Status.MPI_TAG);
	}
	else{
		Finish.Send(JustParam.Results.Status.MPI_SOURCE, JustParam.Results.Status.MPI_TAG);
	}
 }
 printf("Exiting 0\n");
}

void slave(char *StringInput[], int size, int rank){
 _MPI_vector_<unsigned long long> Seed(2);
 Seed.Recv(0, 0);
 _Pi_ Sisi(Seed[0], Seed[1], StringInput[0], StringInput[1]);

 Sisi.Parameters.Recv(0, 0);
 while(Sisi.Parameters[0] > 0.0){
	Sisi.Set_InitialConditions();
	Sisi.Simulate();
//	Sisi.SaveSimulation();
	Sisi.Results.Send(0, 0);
	Sisi.Parameters.SleepRecv(0, 0);
 }
 printf("Exiting, %d\n", rank);
}

int main(int Nargs, char *StringInput[]){

 int rank, size;

 MPI_Init(&Nargs, &StringInput);
 MPI_Comm_size(MPI_COMM_WORLD, &size);
 MPI_Comm_rank(MPI_COMM_WORLD, &rank);

 if(!rank){
	master(&StringInput[1], size, rank);
 }
 else{
	slave(&StringInput[1], size, rank);
 }

 MPI_Finalize();
 return 0;
}

