#ifndef _Parallelize_CondTimeSeries_EXP__H
#define _Parallelize_CondTimeSeries_EXP__H

#include"_Parallelize_CondTimeSeries_.h"

class _Parallelize_CondTimeSeries_Exp_: public _Parallelize_CondTimeSeries_{

	public:
 _Parallelize_CondTimeSeries_Exp_(_Generic_Simulation_*, const char*, int, int);
 _Parallelize_CondTimeSeries_Exp_(_Generic_Simulation_*, int, int);
 inline void AssignTime(_MPI_vector_<double>&, unsigned long long, double tau);
 inline unsigned long long Get_Niter(double, double, double);
};

/******************************************************
	CONSTRUCTORS DESTRUCTORS
******************************************************/
_Parallelize_CondTimeSeries_Exp_::_Parallelize_CondTimeSeries_Exp_(_Generic_Simulation_ *ToParal, const char *filename,int rank, int size):
	_Parallelize_CondTimeSeries_(ToParal, filename, rank, size){
}

_Parallelize_CondTimeSeries_Exp_::_Parallelize_CondTimeSeries_Exp_(_Generic_Simulation_ *ToParal, int rank, int size):
	_Parallelize_CondTimeSeries_(ToParal, rank, size){
}

inline void _Parallelize_CondTimeSeries_Exp_::AssignTime(_MPI_vector_<double> &Parametros, unsigned long long iter, double tau){
// Parametros[3] = iter == 0? Parametros[3] : floor(exp(iter*tau));
 if(iter > 0){
	Parametros[3] *= exp(tau);
	Parametros[3] = floor(Parametros[3]);
 }
}

inline unsigned long long _Parallelize_CondTimeSeries_Exp_::Get_Niter(double TMax, double TMin, double tau){
// unsigned long long Niter = log(TMax/TMin)/tau;
// while(TMax > log(Niter/TMin)/tau){
//	Niter++;
// }
 unsigned long long Niter = 0;
 double TMin_Bef = TMin;
 while(TMin < TMax){
	TMin *= exp(tau);
	TMin = floor(TMin);
	if(TMin == TMin_Bef){
		printf("ERROR: infinite loop in Get_Niter\n");
		exit(1);
	}
	else{
		TMin_Bef = TMin;
	}
	Niter++;
 }
 return Niter;
}












#endif
