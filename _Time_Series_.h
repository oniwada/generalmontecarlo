#ifndef _TIME_SERIES__H
#define _TIME_SERIES__H

#include<iostream>
#include<vector>
#include"_Generic_Simulation_.h"
#include"_MPI_vector_.h"

class _Time_Series_{
 unsigned int Res_SingSize = 0;
 unsigned int Res_ArraySize = 0;
 unsigned int Received_Size = 0;

	public:
 _MPI_vector_<double> Res_Array;
 _MPI_vector_<double> Parameters;
 _MPI_vector_<long long> Id;

 _Time_Series_(_Generic_Simulation_&, unsigned int);

 void Realloc_TimeSeries(unsigned long long);
 void SendAll(unsigned int, unsigned int);
 void Send_TimeSeries(unsigned int, unsigned int);
 void Send_Partial_TimeSeries(unsigned int, unsigned int, unsigned int);
 void Send_Partial_ParamId(int, int, unsigned long long);
 void Send_ParamId(int, int);
 void Recv_ParamId(int, int);
 void SleepRecvAnyAll(void);
 void Recv_TimeSeries(int, int);
 void Set_to_0(void);
 inline void Set_line(_MPI_vector_<double>&, unsigned int);
 inline void Get_line(_MPI_vector_<double>&, unsigned int);
 inline int  Get_Received_size(void); 
 inline unsigned int Get_Res_ArraySize(void);
 inline void Add_line(_MPI_vector_<double>&, unsigned int);

 _Time_Series_ & operator+=(_Time_Series_ &toSum);
};

/*******************************************
	CONSTRUCTOR AND DESTRUCTORS
*******************************************/

_Time_Series_::_Time_Series_(_Generic_Simulation_ &Simul, unsigned int length):
	Res_SingSize(Simul.P2_Res->Get_size()),
	Res_ArraySize(length),
	Res_Array(Simul.P2_Res->Get_size()*length),
	Parameters(Simul.P2_Param->Get_size()),
	Id(length)
{}

void _Time_Series_::Realloc_TimeSeries(unsigned long long size){
 if(Id.Get_size() != size){
	Res_Array.Realloc(Res_SingSize*size);
	Res_ArraySize = size;
	Id.Realloc(size);
 }
}

/*******************************************
	MPI BASED
*******************************************/

void _Time_Series_::SendAll(unsigned int target, unsigned int tag){
 Parameters.Send(target, tag);
 Res_Array.Send(target, tag);
 Id.Send(target, tag);
}

void _Time_Series_::Send_TimeSeries(unsigned int target, unsigned int tag){
 Res_Array.Send(target, tag);
 Id.Send(target, tag);
}

void _Time_Series_::Send_Partial_TimeSeries(unsigned int target, unsigned int tag, unsigned int Partial_size){
 Parameters.Send(target, tag);
 Res_Array.Send_Partial(target, tag, Partial_size*Res_SingSize);
 Id.Send_Partial(target, tag, Partial_size);
}

void _Time_Series_::SleepRecvAnyAll(void){
 Parameters.SleepRecvAny();
 Received_Size = Res_Array.Probe(Parameters.Status.MPI_SOURCE, Parameters.Status.MPI_TAG)/Res_SingSize;
 if(Received_Size > Res_ArraySize){
	do{
		Res_ArraySize *= 2;
	}while(Res_ArraySize < Received_Size);
	Realloc_TimeSeries(Res_ArraySize);
 }
 Res_Array.Recv_Partial(Parameters.Status.MPI_SOURCE, Parameters.Status.MPI_TAG, Received_Size*Res_SingSize);
 Id.Recv_Partial(Parameters.Status.MPI_SOURCE, Parameters.Status.MPI_TAG, Received_Size);
}

void _Time_Series_::Recv_TimeSeries(int source, int tag){
 Received_Size = Res_Array.Probe(source, tag)/Res_SingSize;
 if(Received_Size > Res_ArraySize){
	do{
		Res_ArraySize *= 2;
	}while(Res_ArraySize < Received_Size);
	Realloc_TimeSeries(Res_ArraySize);
 }
 Res_Array.Recv_Partial(source, tag, Received_Size*Res_SingSize);
 Id.Recv_Partial(Parameters.Status.MPI_SOURCE, Parameters.Status.MPI_TAG, Received_Size);
}

void _Time_Series_::Send_ParamId(int target, int tag){
 Parameters.Send(target, tag);
 Id.Send(target, tag);
}

void _Time_Series_::Send_Partial_ParamId(int target, int tag, unsigned long long size){
 Parameters.Send(target, tag);
 Id.Send_Partial(target, tag, size);
}

void _Time_Series_::Recv_ParamId(int source, int tag){
 Parameters.SleepRecv(source, tag);
 Received_Size = Id.Probe(Parameters.Status.MPI_SOURCE, Parameters.Status.MPI_TAG);
 if(Received_Size > Res_ArraySize){
	do{
		Res_ArraySize *= 2;
	}while(Res_ArraySize < Received_Size);
	Realloc_TimeSeries(Res_ArraySize);
 }
 Id.Recv_Partial(Parameters.Status.MPI_SOURCE, Parameters.Status.MPI_TAG, Received_Size);
}

/*******************************************
	GET AND SET
*******************************************/

inline void _Time_Series_::Set_line(_MPI_vector_<double> &toUpdate, unsigned int line){
 std::copy(toUpdate.Get_Pointer(), toUpdate.Get_Pointer() + Res_SingSize, Res_Array.Get_Pointer() + line*Res_SingSize);
}

inline void _Time_Series_::Get_line(_MPI_vector_<double> &toGet, unsigned int line){
 std::copy(Res_Array.Get_Pointer() + Res_SingSize*line, Res_Array.Get_Pointer() + Res_SingSize*line + Res_SingSize, toGet.Get_Pointer());
}

void _Time_Series_::Set_to_0(void){
 Res_Array.Set_to_0();
 Parameters.Set_to_0();
}

inline int _Time_Series_::Get_Received_size(void){
 return Received_Size;
}

inline unsigned int _Time_Series_::Get_Res_ArraySize(void){
 return Res_ArraySize;
}


/*******************************************
	OPERATIONS
*******************************************/

inline void _Time_Series_::Add_line(_MPI_vector_<double> &toSum, unsigned int line){
 double *prt_TS = Res_Array.Get_Pointer() + line*Res_SingSize;
 for(unsigned int index = 0; index < Res_SingSize; index++, prt_TS++){
	*prt_TS += toSum[index];
 }
}

/*******************************************
	OVERLOAD
*******************************************/

_Time_Series_ & _Time_Series_::operator+=(_Time_Series_ & toSum){
 if(toSum.Res_SingSize != Res_SingSize ){
	fprintf(stderr, "Res_SingSize dont match in sum\n");
	exit(1);
 }
 else if(toSum.Res_ArraySize > this->Res_ArraySize){
	unsigned long long PrevSize = Res_Array.Get_size();
	this->Realloc_TimeSeries(toSum.Res_ArraySize);
	memset(Res_Array.Get_Pointer() + PrevSize, 0, sizeof(double)*(Res_Array.Get_size() - PrevSize));
	Res_Array += toSum.Res_Array;
 }
 else{
	for(unsigned long long index = 0UL; index < toSum.Res_Array.Get_size(); index++){
		Res_Array[index] += toSum.Res_Array[index];
	}	
 }
 return *this;
}

#endif
