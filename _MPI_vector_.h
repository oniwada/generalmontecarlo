#ifndef __MPI_VECTOR__H
#define __MPI_VECTOR__H

#include<mpi.h>
#include<string.h>
#include<unistd.h>
#include<iostream>
#include<typeinfo>
#include<stdlib.h>
#include<stdio.h>

template <class VecType>
class _MPI_vector_{
	private:
 unsigned long long size;
 unsigned long long SleepingTime;
 MPI_Datatype _MPI_vector_MPIType;

 void set_MPI_Datatype(void);
 VecType *vec = NULL;
	public:
 MPI_Status Status;

 _MPI_vector_(int);
 _MPI_vector_(_MPI_vector_&);
 _MPI_vector_(void);
 ~_MPI_vector_();
 void Realloc(int);

 inline void Set_to_0(void);
 inline unsigned long long Get_size(void) const;
 unsigned long long Get_SleepingTime(void);
 unsigned long long Set_SleepingTime(unsigned long long);
 inline VecType * Get_Pointer(void) const;
 inline VecType * begin(void) const;
 inline VecType * end(void) const;

 inline void Send(int, int);
 inline void Send_Partial(int, int, int);
 inline void Recv(int, int);
 inline void Recv_Partial(int, int, int);
 void SleepRecv(int, int);
 inline void RecvAny(void);
 void SleepRecvAny(void);
 inline int  Probe(int, int);
 int ProbeSleepAny(void);
 _MPI_vector_* operator+();
 _MPI_vector_& operator*();
 inline _MPI_vector_& operator+=(_MPI_vector_&);
 inline _MPI_vector_& operator-=(_MPI_vector_&);
 inline _MPI_vector_& operator*=(_MPI_vector_&);
 inline _MPI_vector_& operator/=(_MPI_vector_&);
 inline _MPI_vector_& operator+=(VecType);
 inline _MPI_vector_& operator-=(VecType);
 inline _MPI_vector_& operator*=(VecType);
 inline _MPI_vector_& operator/=(VecType);
 inline _MPI_vector_& operator=(_MPI_vector_&);
 inline _MPI_vector_& operator=(VecType);
 inline _MPI_vector_& operator=(VecType *);
 inline VecType operator[](unsigned long long) const;
 inline VecType & operator[](unsigned long long);

 int Print(FILE*, const char*, const char*);
 int Scan(FILE*, const char*, const char*);
};


/* *********************************************

		CONSTRUCTORS
		DESTRUCTORS
		REALLOCATIONS

   ********************************************* */

template <class VecType>
void _MPI_vector_<VecType>::set_MPI_Datatype(void){
 if(typeid(VecType) == typeid(int) ){
	_MPI_vector_MPIType = MPI_INT;
 }
 else if(typeid(VecType) == typeid(unsigned int) ){
	_MPI_vector_MPIType = MPI_UNSIGNED;
 }
 else if(typeid(VecType) == typeid(long) ){
	_MPI_vector_MPIType = MPI_LONG;
 }
 else if(typeid(VecType) == typeid(unsigned long) ){
	_MPI_vector_MPIType = MPI_UNSIGNED_LONG;
 }
 else if(typeid(VecType) == typeid(long long) ){
	_MPI_vector_MPIType = MPI_LONG_LONG;
 }
 else if(typeid(VecType) == typeid(unsigned long long) ){
	_MPI_vector_MPIType = MPI_UNSIGNED_LONG_LONG;
 }
 else if(typeid(VecType) == typeid(float) ){
	_MPI_vector_MPIType = MPI_FLOAT;
 }
 else if(typeid(VecType) == typeid(double) ){
	_MPI_vector_MPIType = MPI_DOUBLE;
 }
 else if(typeid(VecType) == typeid(long double) ){
	_MPI_vector_MPIType = MPI_LONG_DOUBLE;
 }
 else if(typeid(VecType) == typeid(char) ){
	_MPI_vector_MPIType = MPI_CHAR;
 }
 else{
	fprintf(stderr, "No type detected, in _MPI_vector_.h exiting\n");
	exit(1);
 }
}


template <class VecType>
_MPI_vector_<VecType>::_MPI_vector_(int size){

 SleepingTime = 100000;
 if(size < 0){
	fprintf(stderr, "Size in _MPI_vector_ is negative\n");
	exit(EXIT_FAILURE);
 }
 this->size = size;
 vec = (VecType*)malloc(sizeof(VecType)*size);
 if(vec == NULL){
	fprintf(stderr, "vec in _MPI_vector is NULL\n");
	exit(1);
 }
 memset(vec, 0, sizeof(VecType)*size);

 set_MPI_Datatype();

}

template <class VecType>
_MPI_vector_<VecType>::_MPI_vector_(_MPI_vector_<VecType> &toOperate){

 SleepingTime = toOperate.SleepingTime;
 this->size = toOperate.Get_size();
 vec = (VecType*)malloc(sizeof(VecType)*size);
 if(vec == NULL){
	fprintf(stderr, "vec in _MPI_vector is NULL\n");
	exit(1);
 }
 memset(vec, 0, sizeof(VecType)*size);
 set_MPI_Datatype();
 std::copy(toOperate.Get_Pointer(), toOperate.Get_Pointer()+this->size, this->vec);
}

template <class VecType>
_MPI_vector_<VecType>::_MPI_vector_(void){
 SleepingTime = 100000;
 size = 0;
 vec = NULL;
 set_MPI_Datatype();
}

template <class VecType>
_MPI_vector_<VecType>::~_MPI_vector_(void){
 free(vec);
 vec = NULL;
}

template <class VecType>
void _MPI_vector_<VecType>::Realloc(int size){
 this->size = size;
 if(size < 0){
	fprintf(stderr, "Size in _MPI_vector_ is negative\n");
	exit(1);
 }
 this->size = size;
 vec = (VecType*)realloc(vec, sizeof(VecType)*size);
 if(vec == NULL){
	fprintf(stderr, "Failed to Realloc in _MPI_vector\n");
	exit(1);
 }
}


/* *********************************************

		GET PRIVATE DATA

   ********************************************* */


template <class VecType>
inline unsigned long long _MPI_vector_<VecType>::Get_size(void) const{
 return size;
}

template <class VecType>
unsigned long long _MPI_vector_<VecType>::Get_SleepingTime(void){
 return SleepingTime;
}

template <class VecType>
unsigned long long _MPI_vector_<VecType>::Set_SleepingTime(unsigned long long SleepingTime){
 this->SleepingTime = SleepingTime;
 return SleepingTime;
}

template <class VecType>
inline VecType * _MPI_vector_<VecType>::Get_Pointer(void) const{
 return vec;
}

template <class VecType>
inline VecType * _MPI_vector_<VecType>::begin(void) const{
 return Get_Pointer();
}

template <class VecType>
inline VecType * _MPI_vector_<VecType>::end(void) const{
 return Get_Pointer() + this->size;
}

/* *********************************************

		MPI BASED

   ********************************************* */

template <class VecType>
inline void _MPI_vector_<VecType>::Send(int Dest, int Tag){
 MPI_Send(vec, size, _MPI_vector_MPIType, Dest, Tag, MPI_COMM_WORLD);
}

template <class VecType>
inline void _MPI_vector_<VecType>::Send_Partial(int Dest, int Tag, int Truc_message_size){
 MPI_Send(vec, Truc_message_size, _MPI_vector_MPIType, Dest, Tag, MPI_COMM_WORLD);
}

template <class VecType>
inline void _MPI_vector_<VecType>::Recv(int Source, int Tag){
 MPI_Recv(vec, size, _MPI_vector_MPIType, Source, Tag, MPI_COMM_WORLD, &Status);
}

template <class VecType>
inline void _MPI_vector_<VecType>::Recv_Partial(int Source, int Tag, int Truc_message_size){
 MPI_Recv(vec, Truc_message_size, _MPI_vector_MPIType, Source, Tag, MPI_COMM_WORLD, &Status);
}

template <class VecType>
inline void _MPI_vector_<VecType>::RecvAny(void){
 MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &Status);
 MPI_Recv(vec, size, _MPI_vector_MPIType, Status.MPI_SOURCE, Status.MPI_TAG, MPI_COMM_WORLD, &Status);
}

template <class VecType>
void _MPI_vector_<VecType>::SleepRecv(int Source, int Tag){
 int flag = 0;
 MPI_Iprobe(Source, Tag, MPI_COMM_WORLD, &flag, &Status);
 while(!flag){
	usleep(SleepingTime);
	MPI_Iprobe(Source, Tag, MPI_COMM_WORLD, &flag, &Status);
 }
 MPI_Recv(vec, size, _MPI_vector_MPIType, Source, Tag, MPI_COMM_WORLD, &Status);
}

template <class VecType>
void _MPI_vector_<VecType>::SleepRecvAny(void){
 int flag = 0;
 MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &Status);
 while(!flag){
	usleep(SleepingTime);
	MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &Status);
 }
 MPI_Recv(vec, size, _MPI_vector_MPIType, Status.MPI_SOURCE, Status.MPI_TAG, MPI_COMM_WORLD, &Status);
}

template <class VecType>
inline void _MPI_vector_<VecType>::Set_to_0(void){
 memset(vec, 0, sizeof(VecType)*size);
}

template <class VecType>
inline int _MPI_vector_<VecType>::Probe(int source, int tag){
 MPI_Probe(source, tag, MPI_COMM_WORLD, &Status);
 int temp;
 MPI_Get_count(&Status, _MPI_vector_MPIType, &temp);
 return temp;
}

template <class VecType>
int _MPI_vector_<VecType>::ProbeSleepAny(void){
 int flag = 0;
 int temp = 0;
 MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &Status);
 while(!flag){
	usleep(SleepingTime);
	MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &Status);
 }
 MPI_Get_count(&Status, _MPI_vector_MPIType, &temp);
 return temp;
}

/* *********************************************

		OPERATORS

   ********************************************* */

template <class VecType>
_MPI_vector_<VecType>* _MPI_vector_<VecType>::operator+(){
 return this;
}

template <class VecType>
_MPI_vector_<VecType>& _MPI_vector_<VecType>::operator*(){
 return *this;
}

template <class VecType>
inline _MPI_vector_<VecType>& _MPI_vector_<VecType>::operator+=(_MPI_vector_<VecType> &toOperate){
 if(toOperate.Get_size() != size){
	fprintf(stderr, "_MPI_vector_ sizes does not match in adition\n");
	exit(1);
 }
 for(unsigned long long Index = 0; Index < size; Index++){
	vec[Index]+= toOperate.vec[Index];
 }
 return *this;
}

template <class VecType>
inline _MPI_vector_<VecType>& _MPI_vector_<VecType>::operator-=(_MPI_vector_<VecType> &toOperate){
 if(toOperate.Get_size() != size){
	fprintf(stderr, "_MPI_vector_ sizes does not match in subtraction\n");
	exit(1);
 }
 for(unsigned long long Index = 0; Index < size; Index++){
	vec[Index]-= toOperate.vec[Index];
 }
 return *this;
}

template <class VecType>
inline _MPI_vector_<VecType>& _MPI_vector_<VecType>::operator*=(_MPI_vector_<VecType> &toOperate){
 if(toOperate.Get_size() != size){
	fprintf(stderr, "_MPI_vector_ sizes does not match in multiplication\n");
	exit(1);
 }
 for(unsigned long long Index = 0; Index < size; Index++){
	vec[Index]*= toOperate.vec[Index];
 }
 return *this;
}

template <class VecType>
inline _MPI_vector_<VecType>& _MPI_vector_<VecType>::operator/=(_MPI_vector_<VecType> &toOperate){
 if(toOperate.Get_size() != size){
	fprintf(stderr, "_MPI_vector_ sizes does not match in division\n");
	exit(1);
 }
 for(unsigned long long Index = 0; Index < size; Index++){
	vec[Index]/= toOperate.vec[Index];
 }
 return *this;
}


template <class VecType>
inline _MPI_vector_<VecType>& _MPI_vector_<VecType>::operator+=(VecType sum){
 for(unsigned long long Index = 0; Index < size; Index++){
	vec[Index]+= sum;
 }
 return *this;
}


template <class VecType>
inline _MPI_vector_<VecType>& _MPI_vector_<VecType>::operator-=(VecType sub){
 for(unsigned long long Index = 0; Index < size; Index++){
	vec[Index]-= sub;
 }
 return *this;
}


template <class VecType>
inline _MPI_vector_<VecType>& _MPI_vector_<VecType>::operator*=(VecType mult){
 for(unsigned long long Index = 0; Index < size; Index++){
	vec[Index]*= mult;
 }
 return *this;
}

template <class VecType>
inline _MPI_vector_<VecType>& _MPI_vector_<VecType>::operator/=(VecType divisor){
 VecType temp = 1.0/divisor;
 for(unsigned long long Index = 0; Index < size; Index++){
	vec[Index]*= temp;
 }
 return *this;
}

template<class VecType>
inline _MPI_vector_<VecType>& _MPI_vector_<VecType>::operator=(_MPI_vector_ &toOperate){
 if(this->Get_size() != toOperate.Get_size()){
	this->Realloc(toOperate.Get_size());
 }
 std::copy(toOperate.vec, toOperate.vec + toOperate.Get_size(), this->vec);
 return *this;
}

template<class VecType>
inline _MPI_vector_<VecType>& _MPI_vector_<VecType>::operator=(VecType *Value){
 std::copy(Value, Value + this->Get_size(), this->vec);
 return *this;
}


template<class VecType>
inline _MPI_vector_<VecType>& _MPI_vector_<VecType>::operator=(VecType Value){
 for(unsigned long long Index = 0; Index < size; Index++){
	vec[Index] = Value;
 }
 return *this;
}

template<class VecType>
inline VecType _MPI_vector_<VecType>::operator[](unsigned long long Index) const{
 return vec[Index];
}

template<class VecType>
inline VecType & _MPI_vector_<VecType>::operator[](unsigned long long Index){
 return vec[Index];
}

/* *********************************************

		INPUT AND OUTPUT

   ********************************************* */
template<class VecType>
int _MPI_vector_<VecType>::Print(FILE *output, const char* printSpec, const char* spacing){
 unsigned long long Index;
 for(Index = 0; Index < size -1; Index++){
	fprintf(output, printSpec, vec[Index]);
	fprintf(output, spacing);
 }
 return fprintf(output, printSpec, vec[Index]);
}

template<class VecType>
int _MPI_vector_<VecType>::Scan(FILE *input, const char* printSpec, const char* spacing){
 unsigned long long Index;
 for(Index = 0; Index < size -1; Index++){
	if(fscanf(input, printSpec, &vec[Index]) == EOF) return EOF;
	if(fscanf(input, spacing) == EOF) return EOF;
 }
 return fscanf(input, printSpec, &vec[Index]);
}

#endif
