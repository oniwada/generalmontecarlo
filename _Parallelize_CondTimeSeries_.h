#ifndef _Parallelize_CondTimeSeries__H
#define _Parallelize_CondTimeSeries__H

#include<iostream>
#include<mpi.h>
#include<assert.h>
#include<math.h>
#include"_Generic_Simulation_.h"
#include"_Time_Series_.h"

class _Parallelize_CondTimeSeries_{
	private:
 _Generic_Simulation_ *Par_Simul;
 _Time_Series_ TMP_Serie;
 int rank = -1;
 int size = -1;
 unsigned long long NTS_perSlave = 8192;
 FILE *GetParam = NULL;

 void master(void);
 void slave(void);
 void Save_TimeSeries(void);
 unsigned long long Init_Database(_Time_Series_&);
	public:
 inline unsigned long long Get_NTS_perSlave(void);
 inline void Set_NTS_perSlave(unsigned long long);
 _Parallelize_CondTimeSeries_(_Generic_Simulation_*, const char*, int, int);
 _Parallelize_CondTimeSeries_(_Generic_Simulation_*, int, int);
 void run(void);
 void no_means_func(_MPI_vector_<double>&, unsigned long long);
 void no_means_BEFORESEND_func(_MPI_vector_<double>&);
 inline virtual void AssignTime(_MPI_vector_<double>&, unsigned long long, double tau);
 inline virtual unsigned long long Get_Niter(double, double, double);
};

/******************************************************
	CONSTRUCTORS DESTRUCTORS
******************************************************/

_Parallelize_CondTimeSeries_::_Parallelize_CondTimeSeries_(_Generic_Simulation_ *ToParal, const char *filename,int rank, int size):
	TMP_Serie(*ToParal, 32)
{
 Par_Simul = ToParal;
 assert(rank >= 0 && size > 0);
 this->rank = rank;
 this->size = size;
 if( rank == 0 ){
	GetParam = fopen(filename, "r");
	assert(GetParam != NULL && "Error opening file");
 }
}

_Parallelize_CondTimeSeries_::_Parallelize_CondTimeSeries_(_Generic_Simulation_ *ToParal, int rank, int size):
	TMP_Serie(*ToParal, 32)
{
 Par_Simul = ToParal;
 assert(rank >= 0 && size > 0);
 this->rank = rank;
 this->size = size;
}


/******************************************************
	GET AND SET
******************************************************/

inline unsigned long long _Parallelize_CondTimeSeries_::Get_NTS_perSlave(void){
 return NTS_perSlave;
}

inline void _Parallelize_CondTimeSeries_::Set_NTS_perSlave(unsigned long long newNTS_perSlave){
 NTS_perSlave = newNTS_perSlave;
}
/******************************************************
	PARALLELIZE THE SIMULATION
******************************************************/

void _Parallelize_CondTimeSeries_::run(void){
 if( rank == 0){
	master();
 }
 else{
	slave();
 }
}

void _Parallelize_CondTimeSeries_::master(void){
 double NSimul;
 unsigned long long Send_Size = 0;
 _Time_Series_ TMP_Send(*(Par_Simul), 1);
 
 if(TMP_Send.Parameters.Scan(GetParam, "%lf", "%*c") == EOF){
	fprintf(stderr, "nothing to read\n");
	exit(1);
 }
 if(fscanf(GetParam, "%*c%lf", &NSimul) == EOF){
	fprintf(stderr, "nothing to read\n");
	exit(1);
 }
 Send_Size = Init_Database(TMP_Send);
 NSimul = (NSimul - fmod(NSimul, NTS_perSlave))/double(NTS_perSlave);
 for(int Process = 1; Process < size; Process++){
	if(NSimul - Process + 1.0 > 0.0){
		TMP_Send.Send_Partial_ParamId(Process, 0, Send_Size);
	}
	else{
		double MoreSimul;
		if(TMP_Send.Parameters.Scan(GetParam, "%lf", "%*c") == EOF){
			TMP_Send.Parameters[0] = -1.0;
			MoreSimul = -1.0;
		}
		if(fscanf(GetParam, "%*c%lf", &MoreSimul) == EOF){
			TMP_Send.Parameters = -1.0;
			MoreSimul = -1.0;
		}
		if(TMP_Send.Parameters[0] != -1.0){
			Send_Size = Init_Database(TMP_Send);
		}
		if(MoreSimul == -1.0){
			TMP_Send.Send_Partial_ParamId(Process, 0, Send_Size);
			continue;
		}
		MoreSimul = (MoreSimul - fmod(MoreSimul, NTS_perSlave))/double(NTS_perSlave);
		NSimul += MoreSimul;
		if(MoreSimul == 0.0){
			Process--;
			continue;
		}
		TMP_Send.Send_Partial_ParamId(Process, 0, Send_Size);
	}
 }
// if(NSimul == 0.0){
//	TMP.Send.Parameters[0] = -1.0;
// }
// if(NSimul + 1.0 -double(size) >= 0.0){
	NSimul += 1.0 - double(size);
// }
 while(NSimul > 0.1){
	NSimul -= 1.0;
	TMP_Serie.SleepRecvAnyAll();
	TMP_Send.Send_Partial_ParamId(TMP_Serie.Parameters.Status.MPI_SOURCE, TMP_Serie.Parameters.Status.MPI_TAG, Send_Size);
	Save_TimeSeries();
 }
 while(TMP_Send.Parameters.Scan(GetParam, "%lf", "%*c") != EOF){
	if(fscanf(GetParam, "%*c%lf", &NSimul) == EOF) break;
	Send_Size = Init_Database(TMP_Send);
	NSimul = (NSimul - fmod(NSimul, NTS_perSlave))/double(NTS_perSlave);
	while(NSimul > 0.1){
		NSimul -= 1.0;
		TMP_Serie.SleepRecvAnyAll();
		TMP_Send.Send_Partial_ParamId(TMP_Serie.Parameters.Status.MPI_SOURCE, TMP_Serie.Parameters.Status.MPI_TAG, Send_Size);
		Save_TimeSeries();
	}	
 }
 size += NSimul >= 0.1 ? 0.0 : int(NSimul);
 TMP_Send.Parameters[0] = -1.0;
 for(int Process = 1; Process < size; Process++){
	TMP_Serie.SleepRecvAnyAll();
	TMP_Send.Send_Partial_ParamId(TMP_Serie.Parameters.Status.MPI_SOURCE, TMP_Serie.Parameters.Status.MPI_TAG, Send_Size);
	Save_TimeSeries();
 }
 printf("I'm the master process!!\n");
}

/*******************************
0 - L
1 - lambda
2 - p
3 - t / t_init
4 - t_max
5 - tau
*******************************/

void _Parallelize_CondTimeSeries_::slave(void){
 TMP_Serie.Recv_ParamId(0, 0);
 while(TMP_Serie.Parameters[0] >= 0.0){
	(*Par_Simul->P2_Param) = TMP_Serie.Parameters;
#ifdef OTHER_NITER
	unsigned long long Niter = Get_Niter((*Par_Simul->P2_Param)[4], (*Par_Simul->P2_Param)[3], (*Par_Simul->P2_Param)[5]);
#else
#ifndef STATIONARY
	unsigned long long Niter = TMP_Serie.Get_Received_size();
#else
	unsigned long long Niter = Get_Niter((*Par_Simul->P2_Param)[4], (*Par_Simul->P2_Param)[3], (*Par_Simul->P2_Param)[5]);
#endif //STATIONARY
#endif //OTHER_NITER
	double tempt_init = (*Par_Simul->P2_Param)[3];
//	(*Par_Simul->P2_Param)[4] = (*Par_Simul->P2_Param)[3];
	TMP_Serie.Res_Array.Set_to_0();
	for(unsigned long long Realization = 0; Realization < NTS_perSlave; Realization++){
		Par_Simul->Set_Parameters();
		Par_Simul->Set_InitialConditions();
		Par_Simul->Simulate();
		for(unsigned long long iter = 0; iter < Niter; iter++){
			AssignTime(*Par_Simul->P2_Param, iter, (*Par_Simul->P2_Param)[5]);
			Par_Simul->Simulate();
#ifdef NO_MEANS
			no_means_func(*Par_Simul->P2_Res, iter);
#elif !defined(STATIONARY)
			TMP_Serie.Add_line(*Par_Simul->P2_Res, iter);
#else
			TMP_Serie.Add_line(*Par_Simul->P2_Res, 0);
			#define NO_CONDITION
#endif

#ifndef NO_CONDITION
			if( !Par_Simul->Get_SimulationState() ){
				break;
			}
#endif
		}
		(*Par_Simul->P2_Param)[3] = tempt_init;
	}
#ifdef NO_MEANS
	no_means_BEFORESEND_func(*Par_Simul->P2_Res);
#elif !defined(STATIONARY)
	for(unsigned long long x = 0; x < Niter; x++){
		TMP_Serie.Res_Array[x*(Par_Simul->P2_Res->Get_size())] = NTS_perSlave;
	}
	TMP_Serie.Parameters[4] = 0.0;
	TMP_Serie.Parameters[5] = 0.0;
	TMP_Serie.Send_Partial_TimeSeries(0, 0, Niter);
#else
	TMP_Serie.Res_Array /= double(Niter);
	TMP_Serie.Res_Array[0] = NTS_perSlave;
	TMP_Serie.Send_Partial_TimeSeries(0, 0, 1);
#endif
	TMP_Serie.Recv_ParamId(0, 0);
 }
 printf("just the %dth slave ..\n", rank);
 fflush(stdout);
}

#ifndef OTHER_DATABASEMETHOD
void _Parallelize_CondTimeSeries_::Save_TimeSeries(void){
#ifndef STATIONARY
 for(int time = 0; time < TMP_Serie.Get_Received_size(); time++){
	TMP_Serie.Get_line(*Par_Simul->P2_Res, time);
	Par_Simul->Save_Simulation(TMP_Serie.Id[time]);
 }
#else
 TMP_Serie.Get_line(*Par_Simul->P2_Res, 0);
 Par_Simul->Save_Simulation(TMP_Serie.Id[0]);
#endif
}

unsigned long long _Parallelize_CondTimeSeries_::Init_Database(_Time_Series_ &TMP){
#ifndef STATIONARY
 time_t t1, t2;
 time(&t1);
 double t_init, t_end, tau;
 t_init = TMP.Parameters[3];
 t_end = TMP.Parameters[4];
 tau = TMP.Parameters[5];
 TMP.Parameters[4] = 0.0;
 TMP.Parameters[5] = 0.0;
 unsigned long long NStep = Get_Niter(t_end, t_init, tau);
 if(NStep > TMP.Get_Res_ArraySize() ){
	TMP.Realloc_TimeSeries(NStep);
 }
 AssignTime(TMP.Parameters, 0, tau);
 for(unsigned long long time = 0; time < NStep; time++, AssignTime(TMP.Parameters, time, tau)){
	(*Par_Simul->P2_Param) = TMP.Parameters;
	TMP.Id[time] = Par_Simul->Init_Database();
 }
 TMP.Parameters[3] = t_init;
 TMP.Parameters[4] = t_end;
 TMP.Parameters[5] = tau;
 time(&t2);
 printf("%lf\n", difftime(t2, t1));
 return NStep;
#else
 (*Par_Simul->P2_Param) = TMP.Parameters;
 TMP.Id[0] = Par_Simul->Init_Database();
 return 1;
#endif
}
#endif

inline void _Parallelize_CondTimeSeries_::AssignTime(_MPI_vector_<double> &Parametros, unsigned long long iter, double tau){
 Parametros[3] += tau;
}

inline unsigned long long _Parallelize_CondTimeSeries_::Get_Niter(double MaxTime, double MinTime, double tau){
 return (MaxTime-MinTime)/tau;
}












#endif
