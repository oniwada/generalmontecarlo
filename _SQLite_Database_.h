#ifndef _SQLite_Database__h
#define _SQLite_Database__h

#include<iostream>
#include<vector>
#include<stdlib.h>
#include<string>

#ifndef __SQLITEAMALGAMATION__
#include<sqlite3.h>
#else

#ifndef __SQLITEAMALGAMATION__HEADER___
#define __SQLITEAMALGAMATION__HEADER___
#include "sqlite3.h"
#endif //__SQLITEAMALGAMATION__HEADER__

#endif //__SQLITEAMALGAMATION__

namespace _SQLite_Func_{

 struct ColumnNames{
	std::vector<std::string> PrimaryKey;
	std::vector<std::string> Value;
	std::vector<std::string>::iterator it;
	void printAll(void){
		unsigned int i;
		for(i = 0; i < PrimaryKey.size(); i++){
			std::cout << PrimaryKey[i] << std::endl;
		}
		for(i = 0; i < Value.size(); i++){
			std::cout << Value[i] << std::endl;
		}
	}
 };

 int print(void *NotUsed, int Ncolumns, char **Results, char **columns){
	for(int i = 0; i < Ncolumns; i++){
		printf("%s: %s\n", columns[i], Results[i] ? Results[i] : "NULL");
	}
	printf("\n");
	return 0;
 }

 int get_columns(void *Input, int Ncolumns, char **Results, char **columns){
	struct ColumnNames *DatabaseColumns = (ColumnNames*) Input;
	std::string temp;

	bool IsItPrimaryKey = false;
	for(int i = 0; i < Ncolumns; i++){
		std::string Upper_string;
		
		for(char *Iterator = &columns[i][0]; *Iterator; Iterator++) Upper_string += toupper(*Iterator);
		if( !Upper_string.compare("NAME") ){
			if(!strcmp(Results[i], "Id")){
				return 0;
			}
			temp = Results[i];
		}
		else if(!Upper_string.compare("PK") ){
			if(strcmp(Results[i], "0") ){
				IsItPrimaryKey = true;
			}
			else{
				IsItPrimaryKey = false;
			}
		}
//		else if(!Upper_string.compare("TYPE") ){
//			Upper_string.clear();
//			for(char *Iterator = &Results[i][0]; *Iterator; Iterator++) Upper_string += toupper(*Iterator);
//			if( Upper_string.compare("REAL") ){
//				fprintf(stderr, "Table is not made of REAL numbers\n");
//				exit(1);
//			}
//		}
	}
	if(!temp.size()){
		fprintf(stderr, "Error obtaining columns names in _SQLite_Database_\n");
		exit(1);
	}
	else if(IsItPrimaryKey){
		DatabaseColumns->PrimaryKey.push_back(temp);
	}
	else{
		DatabaseColumns->Value.push_back(temp);
	}
	return 0;
 }

 int AssingToLDouble(void *Array, int Ncolumns, char **Results, char **columns){
	long double *buf = (long double*)Array;
	if(buf[Ncolumns] != 0.0){
		buf[Ncolumns] += 1.0;
		return 0;
	}
	int i;
	for(i = 0; i < Ncolumns; i++){
		sscanf(Results[i], "%Lf", &buf[i]);
	}
	buf[Ncolumns] += 1.0;
	return 0;
 }

 int AssingToDouble(void *Array, int Ncolumns, char **Results, char **columns){
	double *buf = (double*)Array;
	long double bbuf;
	if(buf[Ncolumns] != 0.0){
		buf[Ncolumns] += 1.0;
		return 0;
	}
	int i;
	for(i = 0; i < Ncolumns; i++){
		if( !Results[i]){
			buf[i] = 0.0;
		}
		else{
			sscanf(Results[i], "%Lf", &bbuf);
			buf[i] = (double) bbuf;
		}
	}
	buf[Ncolumns] += 1.0;
	return 0;
 }

 int CheckNResults(void *Array, int Ncolumns, char **Results, char **columns){
	int *NValue = (int*) Array;
	NValue[0]++;
	return 0;
 }

}

class _SQLite_Database_{
	private:
 std::string PrintSpec, TableName, PrimaryKey, Value, PrimaryKeyValue, SQLCommand;

 sqlite3 *Database;
 long double PrecisionComp;
 char *ErrMessage;
 long long MaxId = 0UL;

 _SQLite_Func_::ColumnNames DatabaseColumns;
	public:
 _SQLite_Database_(void);
 _SQLite_Database_(const char*, const char*);
 ~_SQLite_Database_(void);
 _SQLite_Database_& Init(const char*, const char*);
 void ReInit(const char*);
 _SQLite_Database_& Exec(const char*);
 _SQLite_Database_& Exec(std::string);
 _SQLite_Database_& Exec(const char*, int (*P2Function)(void*, int, char**, char**) );
 _SQLite_Database_& Exec(std::string, int (*P2Function)(void*, int, char**, char**) );
 _SQLite_Database_& Exec(const char*, int (*P2Function)(void*, int, char**, char**), void* );
 _SQLite_Database_& Exec_noErr(std::string, int (*P2Function)(void*, int, char**, char**), void* );
 _SQLite_Database_& Exec(std::string, int (*P2Function)(void*, int, char**, char**), void* );
 std::string GenerateWhereEqual(long double*);
 std::string GenerateWhereEqual(     double*);
 std::string GenerateSetEqual( long double*);
 std::string GenerateSetEqual(      double*);
 _SQLite_Database_& Insert(double*, double*);
 _SQLite_Database_& Insert(long double*, long double*);
 unsigned int Searchfor(long double*);
 unsigned int Searchfor(     double*);
 unsigned int Searchfor(long double*, long double*);
 unsigned int Searchfor(     double*,      double*);
 unsigned int SearchforId( long long,      double*);
 unsigned int SearchforId( long long, long double*);
 _SQLite_Database_& Update(long double *, long double*);
 _SQLite_Database_& Update(     double *,      double*);
 _SQLite_Database_& UpdatebyId( long long,      double*);
 _SQLite_Database_& UpdatebyId( long long, long double*);
 _SQLite_Database_& InsertId(double*, long long);
 bool CheckTable(const char*);

 double Get_PrecisionComp(void);
 _SQLite_Database_& Set_PrecisionComp(double);
 const char* Get_PrintSpec(void);
 _SQLite_Database_& Set_PrintSpec(const char*);

 inline unsigned int Get_NParam(void);
 inline unsigned int Get_NValues(void);
 inline std::string Get_TableName(void);
 inline std::string Get_Value(void);
 inline std::string Get_PrimaryKey(void);
 inline long long GetSet_MaxId(void);
 long long AssignId(double *);
 long long AssignId(long double *);
};

/* ******************************************

		CONSTRUCTORS
		INITIALIZATOR

   ******************************************  */

_SQLite_Database_::_SQLite_Database_(void):
	Database(NULL)
{}

_SQLite_Database_::_SQLite_Database_(const char* DatabaseName, const char* TableName){
 Init(DatabaseName, TableName);
}

_SQLite_Database_& _SQLite_Database_::Init(const char* DatabaseName, const char* TableName){

 if( sqlite3_open(DatabaseName, &Database) ){
	fprintf(stderr, "Could not open database\n");
	exit(EXIT_FAILURE);
 }
 ReInit(TableName);
 return *this;
}

void _SQLite_Database_::ReInit(const char *TableName){
 PrintSpec.clear();
 this->TableName.clear();
 PrimaryKey.clear();
 Value.clear();
 PrimaryKeyValue.clear();
 SQLCommand.clear();
 DatabaseColumns.PrimaryKey.clear();
 DatabaseColumns.Value.clear();
 this->TableName = TableName;

 std::string temp = "PRAGMA TABLE_INFO(";
 temp += TableName;
 temp += ")";

 Exec(temp.c_str(), _SQLite_Func_::get_columns, (void*) &DatabaseColumns);

 if(DatabaseColumns.PrimaryKey.size() == 0 && DatabaseColumns.Value.size() == 0){
	fprintf(stderr, "There is no columns in table %s\n", TableName);
	exit(EXIT_FAILURE);
 }

 PrimaryKey += "(";
 Value += "(";
 DatabaseColumns.it = DatabaseColumns.PrimaryKey.begin();
 for(; DatabaseColumns.it != DatabaseColumns.PrimaryKey.end() - 1; DatabaseColumns.it++){
	PrimaryKey += *DatabaseColumns.it + ",";
 }
 PrimaryKey += *DatabaseColumns.it;
 PrimaryKeyValue = PrimaryKey;
 PrimaryKey += ")";

 DatabaseColumns.it = DatabaseColumns.Value.begin();
 for(; DatabaseColumns.it != DatabaseColumns.Value.end() - 1; DatabaseColumns.it++){
	Value += *DatabaseColumns.it + ",";
 }
 Value += *DatabaseColumns.it + ")";
 PrimaryKeyValue += Value;
 PrimaryKeyValue[PrimaryKey.size() -1] = ',';
 
 PrecisionComp=1.0 + 1e-14;
 PrintSpec = "%.14Le";
 sqlite3_busy_timeout(Database, 1000000);
}

_SQLite_Database_::~_SQLite_Database_(void){
 sqlite3_close(Database);
}

/* ******************************************

		GET/SET_PRIVATE DATA

   ******************************************  */

double _SQLite_Database_::Get_PrecisionComp(void){
 return PrecisionComp;
}

_SQLite_Database_& _SQLite_Database_::Set_PrecisionComp(double PrecisionComp){
 this->PrecisionComp = PrecisionComp;
 return *this;
}

const char* _SQLite_Database_::Get_PrintSpec(void){
 return (const char*)PrintSpec.c_str();
}

_SQLite_Database_& _SQLite_Database_::Set_PrintSpec(const char* PrintSpec){
 this->PrintSpec = PrintSpec;
 return *this;
}

inline unsigned int _SQLite_Database_::Get_NParam(void){
 return DatabaseColumns.PrimaryKey.size();
}

inline unsigned int _SQLite_Database_::Get_NValues(void){
 return DatabaseColumns.Value.size();
}

inline std::string _SQLite_Database_::Get_TableName(void){
 return TableName;
}

inline std::string _SQLite_Database_::Get_Value(void){
 return Value;
}

inline std::string _SQLite_Database_::Get_PrimaryKey(void){
 return PrimaryKey;
}
/* ******************************************

		COMMANDS GENERATORS

   ******************************************  */

std::string _SQLite_Database_::GenerateWhereEqual(long double *Parameters){
 char buffer[25];
 std::string WhereEqual;
 WhereEqual += "WHERE ";
 unsigned int i;
 for(i = 0; i < Get_NParam() - 1; i++){
	if (Parameters[i] == 0.0){
		WhereEqual += DatabaseColumns.PrimaryKey[i] + "=0.0 AND ";
	}
	else{
		sprintf(buffer, PrintSpec.c_str(), Parameters[i] * PrecisionComp);
		WhereEqual += DatabaseColumns.PrimaryKey[i] + "<" + (std::string) buffer + " AND ";
		sprintf(buffer, PrintSpec.c_str(), Parameters[i] / PrecisionComp);
		WhereEqual += DatabaseColumns.PrimaryKey[i] + ">" + (std::string) buffer + " AND ";
	}
 }
 if (Parameters[i] == 0.0){
	WhereEqual += DatabaseColumns.PrimaryKey[i] + "=0.0";
 }
 else{
	sprintf(buffer, PrintSpec.c_str(), Parameters[i] * PrecisionComp);
	WhereEqual += DatabaseColumns.PrimaryKey[i] + "<" + (std::string) buffer;
	sprintf(buffer, PrintSpec.c_str(), Parameters[i] / PrecisionComp);
	WhereEqual += DatabaseColumns.PrimaryKey[i] + ">" + (std::string) buffer;
 }
 return WhereEqual;
}

std::string _SQLite_Database_::GenerateWhereEqual( double *Parameters){
 char buffer[25];
 std::string WhereEqual;
 WhereEqual += "WHERE ";
 unsigned int i;
 for(i = 0; i < Get_NParam() - 1; i++){
	if (Parameters[i] == 0.0){
		WhereEqual += DatabaseColumns.PrimaryKey[i] + "=0.0 AND ";
	}
	else{
		sprintf(buffer, PrintSpec.c_str(), (long double) Parameters[i] * PrecisionComp);
		WhereEqual += DatabaseColumns.PrimaryKey[i] + "<" + (std::string) buffer + " AND ";
		sprintf(buffer, PrintSpec.c_str(), (long double) Parameters[i] / PrecisionComp);
		WhereEqual += DatabaseColumns.PrimaryKey[i] + ">" + (std::string) buffer + " AND ";
	}
	
 }
 if (Parameters[i] == 0.0){
	WhereEqual += DatabaseColumns.PrimaryKey[i] + "=0.0";
 }
 else{
	sprintf(buffer, PrintSpec.c_str(), (long double) Parameters[i] * PrecisionComp);
	WhereEqual += DatabaseColumns.PrimaryKey[i] + "<" + (std::string) buffer + " AND ";
	sprintf(buffer, PrintSpec.c_str(), (long double) Parameters[i] / PrecisionComp);
	WhereEqual += DatabaseColumns.PrimaryKey[i] + ">" + (std::string) buffer;
 }
 return WhereEqual;
}

std::string _SQLite_Database_::GenerateSetEqual( long double *Data){
 char buffer[25];
 std::string SetEqual = "Set ";
 unsigned int i;
 for(i = 0; i < Get_NValues() -1; i++){
	sprintf(buffer, PrintSpec.c_str(), Data[i]);
	SetEqual += DatabaseColumns.Value[i] + "=" + (std::string) buffer + " ,";
 }
 sprintf(buffer, PrintSpec.c_str(), Data[i]);
 SetEqual += DatabaseColumns.Value[i] + "=" + (std::string) buffer;
 return SetEqual;
}

std::string _SQLite_Database_::GenerateSetEqual( double *Data){
 char buffer[25];
 std::string SetEqual = "Set ";
 unsigned int i;
 for(i = 0; i < Get_NValues() -1; i++){
	sprintf(buffer, PrintSpec.c_str(), (long double) Data[i]);
	SetEqual += DatabaseColumns.Value[i] + "=" + (std::string) buffer + " ,";
 }
 sprintf(buffer, PrintSpec.c_str(), (long double) Data[i]);
 SetEqual += DatabaseColumns.Value[i] + "=" + (std::string) buffer;
 return SetEqual;
}

/* ******************************************

		EXECUTE COMMAND

   ******************************************  */

_SQLite_Database_& _SQLite_Database_::Exec(const char* Command){

 if( sqlite3_exec(Database, Command, NULL, 0, &ErrMessage) != SQLITE_OK){
	fprintf(stderr, "SQL Error: %s\n", ErrMessage);
	fprintf(stderr, "%s\n", Command);
	sqlite3_free(ErrMessage);
	exit(2);
 }
 return *this;
}


_SQLite_Database_& _SQLite_Database_::Exec(std::string Command){

 if( sqlite3_exec(Database, Command.c_str(), NULL, 0, &ErrMessage) != SQLITE_OK){
	fprintf(stderr, "SQL Error: %s\n", ErrMessage);
	fprintf(stderr, "%s\n", Command.c_str());
	sqlite3_free(ErrMessage);
	exit(3);
 }
 return *this;
}

_SQLite_Database_& _SQLite_Database_::Exec(const char* Command, int (*P2Function)(void *, int, char**, char**) ){

 if( sqlite3_exec(Database, Command, P2Function, 0, &ErrMessage) != SQLITE_OK){
	fprintf(stderr, "SQL Error: %s\n", ErrMessage);
	fprintf(stderr, "%s\n", Command);
	sqlite3_free(ErrMessage);
	exit(4);
 }
 return *this;
}

_SQLite_Database_& _SQLite_Database_::Exec(std::string Command, int (*P2Function)(void *, int, char**, char**) ){
 return Exec(Command.c_str(), P2Function);
}

_SQLite_Database_& _SQLite_Database_::Exec(const char* Command, int (*P2Function)(void *, int, char**, char**), void *ArgToFunc ){
 int u = sqlite3_exec(Database, Command, P2Function, ArgToFunc, &ErrMessage);
 if( u != SQLITE_OK){
	fprintf(stderr, "SQL Error: %s %d\n", ErrMessage, u);
	fprintf(stderr, "%s\n", Command);
	sqlite3_free(ErrMessage);
	exit(5);
 }
 return *this;
}

//_SQLite_Database_& _SQLite_Database_::Exec_noErr(std::string Command, int (*P2Function)(void *, int, char**, char**), void *ArgToFunc ){
// sqlite3_exec(Database, Command.c_str(), P2Function, ArgToFunc, &ErrMessage);
// if(ErrMessage != NULL){
//	sqlite3_free(ErrMessage);
// }
// return *this;
//}

_SQLite_Database_& _SQLite_Database_::Exec(std::string Command, int (*P2Function)(void *, int, char**, char**), void *ArgToFunc ){
 return Exec(Command.c_str(), P2Function, ArgToFunc);
}

_SQLite_Database_& _SQLite_Database_::Insert(long double* Parameters, long double* Data){
 SQLCommand.clear();
 char buffer[25];
 SQLCommand += "INSERT INTO " + TableName + PrimaryKeyValue + " VALUES(";
 unsigned int i;
 for(i = 0; i < Get_NParam(); i++){
	sprintf(buffer, PrintSpec.c_str(), Parameters[i]);
	SQLCommand += (std::string) buffer + ",";
 }
 for(i = 0; i < Get_NValues() -1; i++){
	sprintf(buffer, PrintSpec.c_str(), Data[i]);
	SQLCommand += (std::string) buffer + ",";
 }
 sprintf(buffer, PrintSpec.c_str(), Data[i]);
 SQLCommand += (std::string) buffer + ");";
 
 return Exec( SQLCommand);
}

_SQLite_Database_& _SQLite_Database_::Insert(double* Parameters, double* Data){
 SQLCommand.clear();
 char buffer[25];
 SQLCommand += "INSERT INTO " + TableName + PrimaryKeyValue + " VALUES(";
 unsigned int i;
 for(i = 0; i < Get_NParam(); i++){
	sprintf(buffer, PrintSpec.c_str(), (long double) Parameters[i]);
	SQLCommand += (std::string) buffer + ",";
 }
 for(i = 0; i < Get_NValues() -1; i++){
	sprintf(buffer, PrintSpec.c_str(), (long double) Data[i]);
	SQLCommand += (std::string) buffer + ",";
 }
 sprintf(buffer, PrintSpec.c_str(), (long double) Data[i]);
 SQLCommand += (std::string) buffer + ");";
 
 return Exec( SQLCommand);
}

_SQLite_Database_& _SQLite_Database_::Update(long double *Parameters, long double *Data){
 SQLCommand.clear();
 SQLCommand += "UPDATE " + TableName + " " + GenerateSetEqual(Data) + " " + GenerateWhereEqual(Parameters) + ";";
 return Exec(SQLCommand);
}

_SQLite_Database_& _SQLite_Database_::Update(double *Parameters, double *Data){
 SQLCommand.clear();
 SQLCommand += "UPDATE " + TableName + " " + GenerateSetEqual(Data) + " " + GenerateWhereEqual(Parameters) + ";";
 return Exec(SQLCommand);
}

unsigned int _SQLite_Database_::Searchfor(long double *Parameters, long double *Data){

 SQLCommand.clear();
 SQLCommand += "SELECT "+ Value.substr(1, Value.size()-2) +" FROM " + TableName + " " + GenerateWhereEqual(Parameters) + ";";

 std::vector<double> temp(Get_NValues()+1, 0.0);// should it be std::vector<long double> ??
// for(unsigned int i = 0; i < Get_NValues()+1; i ++) temp.push_back(0.0);
 Exec(SQLCommand, _SQLite_Func_::AssingToLDouble, (void*) &temp[0]);
 std::copy(temp.begin(), temp.end() -1 , Data);

 return temp[Get_NValues()];
}

unsigned int _SQLite_Database_::Searchfor(long double *Parameters){

 SQLCommand.clear();
 SQLCommand += "SELECT "+ Value.substr(1, Value.size()-2) +" FROM " + TableName + " " + GenerateWhereEqual(Parameters) + ";";

 std::vector<double> temp(Get_NValues()+1, 0.0);// should it be std::vector<long double> ??
// for(unsigned int i = 0; i < Get_NValues()+1; i ++) temp.push_back(0.0);
 Exec(SQLCommand, _SQLite_Func_::AssingToLDouble, (void*) &temp[0]);

 return temp[Get_NValues()];
}

unsigned int _SQLite_Database_::Searchfor(double *Parameters, double *Data){

 SQLCommand.clear();
 SQLCommand += "SELECT "+ Value.substr(1, Value.size()-2) +" FROM " + TableName + " " + GenerateWhereEqual(Parameters) + ";";

 std::vector<double> temp(Get_NValues()+1, 0.0);
// for(unsigned int i = 0; i < Get_NValues()+1; i ++) temp.push_back(0.0);
 Exec(SQLCommand, _SQLite_Func_::AssingToDouble, (void*) &temp[0]);
 std::copy(temp.begin(), temp.end() -1 , &Data[0]);
 
 return temp[Get_NValues()];
}

unsigned int _SQLite_Database_::Searchfor(double *Parameters){

 SQLCommand.clear();
 SQLCommand += "SELECT "+ Value.substr(1, Value.size()-2) +" FROM " + TableName + " " + GenerateWhereEqual(Parameters) + ";";

 std::vector<double> temp(Get_NValues()+1, 0.0);
// for(unsigned int i = 0; i < Get_NValues()+1; i ++) temp.push_back(0.0);
 Exec(SQLCommand, _SQLite_Func_::AssingToDouble, (void*) &temp[0]);

 return temp[Get_NValues()];
}

/* ******************************************

		Id BASED

   ******************************************  */

inline long long _SQLite_Database_::GetSet_MaxId(void){
 double temp[2] = {0.0};
 Exec("SELECT MAX(Id) FROM " + TableName + ";", _SQLite_Func_::AssingToDouble, (void*) temp);
 MaxId = temp[0];
 return MaxId;
}

unsigned int _SQLite_Database_::SearchforId(long long Id, double *Data){
 SQLCommand.clear();
 SQLCommand += "SELECT "+ Value.substr(1, Value.size()-2) +" FROM " + TableName + " WHERE Id="+ std::to_string(Id) +";";

 std::vector<double> temp(Get_NValues()+1, 0.0);

 Exec(SQLCommand, _SQLite_Func_::AssingToDouble, (void*) &temp[0]);
 std::copy(temp.begin(), temp.end() -1 , &Data[0]);
 
 return temp[Get_NValues()];
}

unsigned int _SQLite_Database_::SearchforId(long long Id, long double *Data){

 SQLCommand.clear();
 SQLCommand += "SELECT "+ Value.substr(1, Value.size()-2) +" FROM " + TableName + " WHERE Id="+ std::to_string(Id) +";";

 std::vector<double> temp(Get_NValues()+1, 0.0);
 Exec(SQLCommand, _SQLite_Func_::AssingToLDouble, (void*) &temp[0]);
 std::copy(temp.begin(), temp.end() -1 , Data);

 return temp[Get_NValues()];
}

_SQLite_Database_& _SQLite_Database_::UpdatebyId(long long Id, long double *Data){
 SQLCommand.clear();
 SQLCommand += "UPDATE " + TableName + " " + GenerateSetEqual(Data) + " WHERE Id=" +std::to_string(Id) + ";";
 return Exec(SQLCommand);
}

_SQLite_Database_& _SQLite_Database_::UpdatebyId(long long Id, double *Data){
 SQLCommand.clear();
 SQLCommand += "UPDATE " + TableName + " " + GenerateSetEqual(Data) + " WHERE Id="+std::to_string(Id) + ";";
 return Exec(SQLCommand);
}

long long _SQLite_Database_::AssignId(double *Parameters){
 SQLCommand.clear();
 SQLCommand += "SELECT Id FROM " + TableName + " " + GenerateWhereEqual(Parameters) + " LIMIT 1;";
 double temp[2] = {0.0};
 Exec(SQLCommand, _SQLite_Func_::AssingToDouble, (void*) temp);
// std::cout<<SQLCommand<<std::endl;
// printf("%lf %lf\n", temp[0], temp[1]);
 if(temp[1] == 0.0){
	InsertId(Parameters, ++MaxId);
	return MaxId;
 }
 else{
	return temp[0];
 }
}

//long long _SQLite_Database_::AssignId(long double *Parameters){
// SQLCommand.clear();
// SQLCommand += "SELECT Id FROM " + TableName + " " + GenerateWhereEqual(Parameters) + ";";
// long double temp[2] = {0.0};
// Exec(SQLCommand, _SQLite_Func_::AssingToDouble, (void*) temp);
// if(temp[1] == 0.0){
//	InsertId(Parameters, ++MaxId);
//	return MaxId;
// }
// else{
//	return temp[0];
// }
//}


_SQLite_Database_& _SQLite_Database_::InsertId(double* Parameters, long long Id){
 SQLCommand.clear();
 char buffer[25];
 SQLCommand += "INSERT INTO " + TableName + " " + PrimaryKeyValue.substr(0,PrimaryKeyValue.size() -1) + ",Id) VALUES(";
 unsigned int i;
 for(i = 0; i < Get_NParam(); i++){
	sprintf(buffer, PrintSpec.c_str(), (long double) Parameters[i]);
	SQLCommand += (std::string) buffer + ",";
 }
 for(i = 0; i < Get_NValues(); i++){
	SQLCommand += "0,";
 }
 sprintf(buffer, PrintSpec.c_str(), (long double) Id);
 SQLCommand += (std::string) buffer + ");";
// std::cout<< SQLCommand << std::endl;
 return Exec( SQLCommand);
}

bool _SQLite_Database_::CheckTable(const char *TobeChecked){
 SQLCommand.clear();
 SQLCommand += "SELECT name FROM sqlite_master WHERE type='table' AND name='" + (std::string) TobeChecked + "';";
 int NResults = 0;
 int *PtoNResults = &NResults;
 Exec(SQLCommand, _SQLite_Func_::CheckNResults, (void*) PtoNResults);
 return NResults ? true : false;
}
#endif
